const helloWorld = (name) => {
    return new Promise((resolve, reject) => {
      if (name === "" || name === undefined || name === null) {
        reject("You have to provide the name!");
      } else {
        var message = "Hello world, " + name;
        resolve(message);
      }
    });
};

const addCodepolitan = (message) => {
    return message + " from codepolitan";
}
helloWorld("muhammad fathony")
.then((result) => {
// console.log(`aku ${result}`)
return result;
})
.then((result2)=> {
    var message = addCodepolitan(result2);
    console.log(message)
})
.catch((error) => {
    console.log(error);
});

var promises = Promise.all([
    helloWorld("test 123"),
    helloWorld("s"),
    helloWorld("muhammad")
    .then((result) => {
        return result;
    })
    .then((result2) => {
        var message = addCodepolitan(result2);
        return message;
    })
]);

promises.then((results) => {
    console.log(results);
})

const request = new XMLHttpRequest();

request.onload = function(){
    const data = JSON.parse(this.responseText);
    //console.log(data)
}

request.onerror = function() {
    console.log('Error : -S', error);
}
request.open('get', 'https://readerapi.codepolitan.com/articles', true);
request.send();

fetch('https://readerapi.codepolitan.com/articles')
    .then((response) => {
        if(response.status !== 200){
            console.log('Error : ' + response.status);
            return;
        }

        response.json().then((data) =>{
            //console.log(data);
        })
    }).catch((error) => {
        console.log('Error: ' + error)
    })

fetch('https://readerapi.codepolitan.com/articles')
    .then( (response) => {
        if (response.status !== 200) {
            console.log('Error : ' + response.status);
            // Method reject() akan membuat blok catch terpanggil
            return Promise.reject(new Error(response.statusText));
        } else {
            // Mengubah suatu objek menjadi Promise agar bisa "di-then-kan"
            return Promise.resolve(response);
        }
    }).then((response) => {
        // Mengembalikan sebuah Promise berupa objek/array JavaScript
        // yang diubah dari teks JSON. 
        return response.json();
    }).then((data) => {
        // Objek/array JavaScript dari response.json() masuk lewat data.
        //console.log(data);
    }).catch((error) => {
        // Parameter error berasal dari Promise.reject() 
        console.log('Error : ' + error);
    });

const status = (response) => {
    if(response.status !== 200){
        console.log('Error :' + response.status)
        return Promise.reject(new Error(response.statusText))
    } else {
        return Promise.resolve(response)
    }
}

const json = (response) => response.json();

const error = (error) => console.log('Error :' + error);

fetch('https://readerapi.codepolitan.com/articles')
    .then(status)
    .then(json)
    .then((data) => {
        //console.log(data);
    })
    .catch(error);