//setting
const dbPromised = idb.open("news-reader", 1, function(upgradeDB){
    const articlesObjectStore = upgradeDB.createObjectStore("articles", {
        keyPath: "ID"
    });
    articlesObjectStore.createIndex("post_title", "post_title", { unique : false});
});
//add
const saveForlater = (article) => {
    dbPromised
        .then((db) => {
            var tx = db.transaction("articles", "readwrite");
            var store = tx.objectStore("articles");
            console.log(article)
            store.add(article.result);
            return tx.complete;
        })
        .then(() => console.log("Artikel berhasil di simpan"));
}
//get
const getAll = () => {
    return new Promise ((resolve, reject) => {
        dbPromised
            .then((db) => {
                var  tx = db.transaction("articles", "readonly");
                var store = tx.objectStore("articles");
                return store.getAll();
            })
            .then((articles) => resolve(articles));
    });
}

//index db
// var dbPromise = idb.open("mydatabase", 1, function(upgradeDb) {
//     if (!upgradeDb.objectStoreNames.contains("events")) {
//       upgradeDb.createObjectStore("events");
//     }
    //Key path adalah properti di mana kita mendefinisikan sendiri data apa yang akan bertindak sebagai primary key.
    //upgradeDb.createObjectStore('buku', {keyPath: 'isbn'});
    //Kita juga bisa menggunakan key generator yang bisa memastikan sebuah properti memiliki nilai yang berbeda karena di autoIncrement-kan
    //upgradeDb.createObjectStore('buku', {autoIncrement:true});
    
//});

var dbPromise = idb.open("perpustakaan", 1, function(upgradeDb) {
    if (!upgradeDb.objectStoreNames.contains("buku")) {
      var peopleOS = upgradeDb.createObjectStore("buku", { keyPath: "isbn" });
      peopleOS.createIndex("judul", "judul", { unique: false });
      peopleOS.createIndex("nomorIndux", "nomorIndux", { unique: true });
    }
  
});
dbPromise.then(function(db) {
    var tx = db.transaction('buku', 'readwrite');
    var store = tx.objectStore('buku');
    var item = {
        judul: 'Menjadi Android Developer Expert (MADE)',
        isbn: 123456789,
        description: 'Belajar pemrograman Android di Dicoding dengan modul online dan buku.',
        created: new Date().getTime()
    };
    store.add(item); //menambahkan key "buku"
    return tx.complete;
}).then(function() {
    console.log('Buku berhasil disimpan.');
}).catch(function() {
    console.log('Buku gagal disimpan.')
})
 ///get
dbPromise.then(function(db) {
    var tx = db.transaction('buku', 'readonly');
    var store = tx.objectStore('buku');
    // mengambil primary key berdasarkan isbn
    return store.get(123456789); 
  }).then(function(val) {
    console.dir(val);
  });
  dbPromise.then(function(db) {
    var tx = db.transaction('buku', 'readonly');
    var store = tx.objectStore('buku');
    return store.getAll();
  }).then(function(items) {
    console.log('Data yang diambil: ');
    console.log(items);
  });

  //update
//   dbPromise.then(function(db) {
//     var tx = db.transaction('buku', 'readwrite');
//     var store = tx.objectStore('buku');
//     var item = {
//         judul: 'Menjadi Android Developer Expert (MADE)',
//         isbn: 123456789,
//         description: 'Belajar pemrograman Android di Dicoding dengan modul online dan buku.',
//         created: new Date().getTime()
//     };
//     store.put(item, 123456789); //menambahkan KEY
//     return tx.complete;
// }).then(function() {
//     console.log('Buku berhasil disimpan.');
// }).catch(function() {
//     console.error('Buku gagal disimpan.')
// })
dbPromise.then(function(db) {
    var tx = db.transaction('buku', 'readwrite');
    var store = tx.objectStore('buku');
    store.delete('123456789');
    return tx.complete;
  }).then(function() {
    console.log('Item deleted');
  });